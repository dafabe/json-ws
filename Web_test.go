package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func Example_json() {


	go handleRequests()
	//Encode the data
	postBody, _ := json.Marshal(map[string]string{
		"name":  "Dave",
		"email": "dave@example.com",
	})
	fakeDataGoingIn := bytes.NewBuffer(postBody)
	//Leverage Go's HTTP Post function to make request
	resp, err := http.Post("http://localhost:10000/json_test", "application/json", fakeDataGoingIn)
	//Handle Error
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()
	//Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//sb := string(body)
	b := bytes.NewBuffer(body)

	var v Test_struct
	json.NewDecoder(b).Decode(&v)
	fmt.Println(v)
    // Output:
    // sdf
}