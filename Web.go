package main

import (
	"encoding/json"
	"log"
	"net/http"
)
type Test_struct struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}
func handleRequests() {
	http.HandleFunc("/json_test", homePage)
	log.Fatal(http.ListenAndServe(":10000", nil))
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
func homePage(w http.ResponseWriter, r *http.Request) {

	enableCors(&w)
	decoder := json.NewDecoder(r.Body)
	var t Test_struct
	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}
	json.NewEncoder(w).Encode(&t)
}

